-- global variables
capmode = false
ddmode = false
placeholdermode = false
tpmodes = {'normal', 'dt', 'treasure'}
tpmodeindex = 1;
queuedset = {}
isqueued = false

function get_sets()
    sets.idle = {}                  -- Leave this empty
    sets.precast = {}               -- Leave this empty    
    sets.midcast = {}               -- Leave this empty    
    sets.aftercast = {}             -- Leave this empty
    sets.engaged = {}				-- Leave this empty
    sets.ws = {}					-- Leave this empty
    sets.ja = {}					-- Leave this empty
    sets.special = {}				-- Leave this empty
    sets.weapons = {}				-- Leave this empty

    -- JSE capes
    intarabus={}
    intarabus.fastcast = { name="Intarabus's Cape", augments={'CHR+20','Mag. Acc+20 /Mag. Dmg.+20','"Fast Cast"+10',}}
    intarabus.dualwield = { name="Intarabus's Cape", augments={'DEX+20','Accuracy+20 Attack+20','"Dual Wield"+10',}}
   
    -- weapon sets
    sets.weapons.tauret = {
    	name="Tauret",
	    main="Tauret",
    }

    sets.weapons.kali = {
    	name="Kali",
    	main={ name="Kali", augments={'DMG:+15','CHR+15','Mag. Acc.+15',}},
    }

    sets.weapons.daybreak = {
    	name="Daybreak",
	    main="Daybreak",
    }

    sets.weapons.tplinos = {
	    range={ name="Linos", augments={'Accuracy+13','"Store TP"+3','Quadruple Attack +3',}},
    }

    sets.weapons.songpotency = {
    	main={ name="Kali", augments={'DMG:+15','CHR+15','Mag. Acc.+15',}},
    	range="Gjallarhorn",
    }

    sets.weapons.placeholder = {
    	main={ name="Kali", augments={'DMG:+15','CHR+15','Mag. Acc.+15',}},
    	range="Terpander",
    }

    sets.weapons.hordelullaby = {
    	main={ name="Kali", augments={'DMG:+15','CHR+15','Mag. Acc.+15',}},
    	range="Blurred Harp +1",
    }

    sets.weapons.default = set_combine(sets.weapons.tauret, sets.weapons.tplinos)

    -- idle sets
    sets.idle.normal = {
	    head="Inyanga Tiara +1",
	    body="Inyanga Jubbah +1",
	    hands="Inyan. Dastanas +2",
	    legs="Inyanga Shalwar +2",
	    feet="Fili Cothurnes +1",
	    neck={ name="Bathy Choker +1", augments={'Path: A',}},
	    waist="Svelt. Gouriz +1",
	    left_ear="Eabani Earring",
	    right_ear="Infused Earring",
	    left_ring="Warden's Ring",
	    right_ring="Defending Ring",
	    back="Dew Silk Cape +1"
    }

    sets.idle.dt = set_combine(sets.idle.normal, {
	    right_ring="Defending Ring",
    })

    -- precast sets
    sets.precast.bardsong = {
	    head="Fili Calot +1",
	    body="Inyanga Jubbah",
    	hands={ name="Gende. Gages +1", augments={'Phys. dmg. taken -4%','Song spellcasting time -5%',}},
	    legs="Gyve Trousers",
	    feet="Brioso Slippers +2",
	    neck="Orunmila's Torque",
	    waist="Embla Sash",
	    left_ear="Loquac. Earring",
	    right_ear="Enchntr. Earring +1",
	    left_ring="Kishar Ring",
	    right_ring="Defending Ring",
	    back=intarabus.fastcast,
    }

    sets.precast.fastcast = {
        head="Nahtirah Hat",
	    body="Inyanga Jubbah",
    	hands={ name="Gende. Gages +1", augments={'Phys. dmg. taken -4%','Song spellcasting time -5%',}},
	    legs="Gyve Trousers",
	    feet="Brioso Slippers +2",
	    neck="Orunmila's Torque",
	    waist="Embla Sash",
	    left_ear="Loquac. Earring",
	    right_ear="Enchntr. Earring +1",
	    left_ring="Kishar Ring",
	    right_ring="Defending Ring",
	    back=intarabus.fastcast,
    }

    -- midcast sets

    sets.midcast.placeholder = {
	    head="Aya. Zucchetto +1",
	    body="Weather. Robe +1",
	    hands="Aya. Manopolas +1",
	    legs="Weath. Pants +1",
	    feet="Aya. Gambieras +1",
	    neck="Chivalrous Chain",
	    waist="Svelt. Gouriz +1",
	    left_ear="Eabani Earring",
	    right_ear="Enchntr. Earring +1",
	    left_ring="Chirich Ring",
	    right_ring="Defending Ring",
	    back="Dew Silk Cape +1",
    }

    sets.midcast.bardsong = {
    	main={ name="Kali", augments={'DMG:+15','CHR+15','Mag. Acc.+15',}},
    	range="Gjallarhorn",
	    head="Fili Calot +1",
	    body="Fili Hongreline +2",
	    hands="Fili Manchettes +1",
	    legs="Inyanga Shalwar +2",
	    feet="Brioso Slippers +2",
	    neck="Mnbw. Whistle +1",
	    waist="Svelt. Gouriz +1",
	    left_ear="Eabani Earring",
	    right_ear="Infused Earring",
	    left_ring="Warden's Ring",
	    right_ring="Defending Ring",
	    back=intarabus.fastcast,
    }

    sets.midcast.bardsong.debuff = {
	    head="Inyanga Tiara +1",
	    body="Inyanga Jubbah +1",
	    hands="Fili Manchettes +1",
	    legs="Inyanga Shalwar +2",
	    feet="Brioso Slippers +2",
	    neck="Mnbw. Whistle +1",
	    waist="Eschan Stone",
	    left_ear="Enchntr. Earring +1",
	    right_ear="Gwati Earring",
	    left_ring="Stikini Ring",
	    right_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    back=intarabus.fastcast,
    }

    sets.midcast.bardsong.foelullaby = {
	    head="Inyanga Tiara +1",
	    body="Inyanga Jubbah +1",
	    hands="Fili Manchettes +1",
	    legs="Inyanga Shalwar +2",
	    feet="Brioso Slippers +2",
	    neck="Mnbw. Whistle +1",
	    waist="Eschan Stone",
	    left_ear="Enchntr. Earring +1",
	    right_ear="Gwati Earring",
	    left_ring="Stikini Ring",
	    right_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    back=intarabus.fastcast,
    }

    sets.midcast.bardsong.hordelullaby1 = {
	    head="Inyanga Tiara +1",
	    body="Inyanga Jubbah +1",
	    hands="Fili Manchettes +1",
	    legs="Inyanga Shalwar +2",
	    feet="Brioso Slippers +2",
	    neck="Mnbw. Whistle +1",
	    waist="Eschan Stone",
	    left_ear="Enchntr. Earring +1",
	    right_ear="Gwati Earring",
	    left_ring="Stikini Ring",
	    right_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    back=intarabus.fastcast,
    }

    sets.midcast.bardsong.hordelullaby2 = {
	    head="Inyanga Tiara +1",
	    body="Inyanga Jubbah +1",
	    hands="Fili Manchettes +1",
	    legs="Inyanga Shalwar +2",
	    feet="Brioso Slippers +2",
	    neck="Mnbw. Whistle +1",
	    waist="Eschan Stone",
	    left_ear="Gersemi Earring",
	    right_ear="Gwati Earring",
	    left_ring="Stikini Ring",
	    right_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    back=intarabus.fastcast,
    }

    sets.midcast.cure = {
    	main="Daybreak",
	    head={ name="Vanya Hood", augments={'MND+10','Spell interruption rate down +15%','"Conserve MP"+6',}},
        body={ name="Kaykaus Bliaut", augments={'MP+60','"Cure" potency +5%','"Conserve MP"+6',}},
    	hands={ name="Kaykaus Cuffs", augments={'MP+60','"Conserve MP"+6','"Fast Cast"+3',}},	
	    legs={ name="Vanya Slops", augments={'MND+10','Spell interruption rate down +15%','"Conserve MP"+6',}},
	    feet={ name="Vanya Clogs", augments={'"Cure" potency +5%','"Cure" spellcasting time -15%','"Conserve MP"+6',}},
	    neck="Reti Pendant",
	    left_ring="Stikini Ring",
    }

	sets.midcast.enhancing = {
	    head={ name="Telchine Cap", augments={'Enh. Mag. eff. dur. +10',}},
	    body={ name="Telchine Chas.", augments={'Enh. Mag. eff. dur. +9',}},
	    hands={ name="Telchine Gloves", augments={'Enh. Mag. eff. dur. +8',}},
	    legs={ name="Telchine Braconi", augments={'"Store TP"+4','Enh. Mag. eff. dur. +9',}},
	    feet={ name="Telchine Pigaches", augments={'Enh. Mag. eff. dur. +9',}},
		waist="Embla Sash",
	}

    sets.midcast.elemental = {

    }

	sets.midcast.enfeebling = {
	    head="Aya. Zucchetto +2",
	    body="Inyanga Jubbah +1",
	    hands="Inyan. Dastanas +2",
	    legs="Inyanga Shalwar +2",
	    feet="Brioso Slippers +2",
	    neck="Mnbw. Whistle +1",
	    waist="Eschan Stone",
	    left_ear="Hermetic Earring",
	    right_ear="Gwati Earring",
	    left_ring="Stikini Ring",
	    right_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    back=intarabus.fastcast,
	}

	sets.midcast.enmity = {

	}

	-- engaged sets
    sets.engaged.normal = {
	    head="Aya. Zucchetto +2",
	    body={ name="Bihu Jstcorps +2", augments={'Enhances "Troubadour" effect',}},
	    hands="Volte Mittens",
	    legs={ name="Bihu Cannions +1", augments={'Enhances "Soul Voice" effect',}},
	    feet="Aya. Gambieras +1",
	    neck="Bard's Charm +1",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Brutal Earring",
	    right_ear="Cessance Earring",
	    left_ring="Chirich Ring",
	    right_ring="Petrov Ring",
	    back=intarabus.dualwield,
    }

	sets.engaged.dt = set_combine(sets.engaged.normal, {
		right_ring="Defending Ring",
		neck="Elite royal collar",
	})

	sets.engaged.treasure = set_combine(sets.engaged.normal, {
		ammo="Per. Lucky Egg",
		head="Wh. Rarab Cap +1",
		waist="Chaac Belt",
	})

	-- job ability sets
	sets.ja['Nightingale'] = set_combine(sets.idle.dt, {
	    feet={ name="Bihu Slippers +1", augments={'Enhances "Nightingale" effect',}},
	})
	
	sets.ja['Troubadour'] = set_combine(sets.idle.dt, {
    	body={ name="Bihu Jstcorps +2", augments={'Enhances "Troubadour" effect',}},
	})

	sets.ja['Soul Voice'] = set_combine(sets.idle.dt, {
    	legs={ name="Bihu Cannions +1", augments={'Enhances "Soul Voice" effect',}},
	})

	-- weapon skill sets
	sets.ws.debuff = {

	}

	sets.ws.magic = {
	    head="Aya. Zucchetto +2",
	    body="Weather. Robe +1",
	    hands="Aya. Manopolas +1",
	    legs="Gyve Trousers",
	    feet="Weath. Souliers +1",
	    neck="Sibyl Scarf",
	    waist="Eschan Stone",
	    left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    right_ear="Friomisi Earring",
	    left_ring="Chirich Ring",
	    right_ring="Petrov Ring",
	    back=intarabus.fastcast,
	}

	sets.ws['Evisceration'] = {
	    head="Aya. Zucchetto +1",
	    body={ name="Bihu Jstcorps +2", augments={'Enhances "Troubadour" effect',}},
	    hands="Aya. Manopolas +1",
	    legs={ name="Bihu Cannions +1", augments={'Enhances "Soul Voice" effect',}},
	    feet="Aya. Gambieras +1",
	    neck="Fotia Gorget",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Brutal Earring",
	    right_ear="Mache Earring +1",
	    left_ring="Chirich Ring",
	    right_ring="Petrov Ring",
	    back=intarabus.dualwield,
	}

	sets.ws['Rudra\'s Storm'] = {
	    head="Aya. Zucchetto +1",
	    body={ name="Bihu Jstcorps +2", augments={'Enhances "Troubadour" effect',}},
	    hands="Aya. Manopolas +1",
	    legs={ name="Bihu Cannions +1", augments={'Enhances "Soul Voice" effect',}},
	    feet="Aya. Gambieras +1",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Brutal Earring",
	    right_ear="Mache Earring +1",
	    left_ring="Chirich Ring",
	    right_ring="Petrov Ring",
	    back=intarabus.dualwield,
	}

	sets.ws['Aeolian Edge'] = sets.ws.magic


    sets.special.capacitypoints = {
    	back="Aptitude Mantle +1"
    }
 
end
 
function precast(spell)
	if spell.type == "BardSong" then
		precast_bardsong(spell)	
	elseif spell.type == "Ninjutsu" or spell.type == "WhiteMagic" or spell.type == "BlackMagic" or spell.type == "Trust" then
		equip (sets.precast.fastcast)
	end
end

function precast_bardsong(spell)
	equip(sets.precast.bardsong)
	if placeholdermode then
		equip(sets.weapons.placeholder)
	elseif spell.english:startswith("Horde") then
		equip(sets.weapons.hordelullaby)
	else
		equip(sets.weapons.songpotency)
	end
end
 
function midcast(spell)
	if spell.type == "BardSong" then
		midcast_bardsong(spell)
	elseif spell.type == "WhiteMagic" or spell.type == "BlackMagic" then
		midcast_magic(spell)
	elseif spell.type == "JobAbility" then
		if sets.ja[spell.english] then
			equip(sets.ja[spell.english])
		end
	elseif spell.type == "WeaponSkill" then
		if sets.ws[spell.english] then
			equip(sets.ws[spell.english])
		end
	end
end

function midcast_bardsong(spell)
	if (placeholdermode) then
		equip(sets.midcast.placeholder)
	elseif spell.english:endswith("Threnody") 
		or spell.english:endswith("Threnody II")
		or spell.english:endswith("Elegy") 
		or spell.english == "Magic Finale"
		or spell.english == "Pining Nocturne"
		or spell.english == "Maiden\"s Virelai" then
		equip(sets.midcast.bardsong.debuff)
	elseif spell.english:startswith("Foe Lullaby") then
		equip(sets.midcast.bardsong.foelullaby)
	elseif spell.english == "Horde Lullaby" then
		equip(sets.midcast.bardsong.hordelullaby1)
	elseif spell.english == "Horde Lullaby II" then
		equip(sets.midcast.bardsong.hordelullaby2)
	else
		equip(sets.midcast.bardsong)
	end
end

function midcast_magic(spell)
	if spell.english:startswith("Cure")
		or spell.english:startswith("Cura") then
		equip (sets.midcast.cure)
	elseif spell.skill == "Enhancing Magic" then
		equip(sets.midcast.enhancing)
	elseif spell.skill == "Enfeebling Magic" then
		equip(sets.midcast.enfeebling)
	end
end

function aftercast(spell)
	handle_status_change()
	if spell.type == "BardSong" then
		equip(sets.weapons.default)
	end
end
 
function status_change(new,old)
	handle_status_change()
end

function handle_status_change()
	if player.status == "Engaged" then
		equip_tp_set()
	else
		equip_idle_set()
	end
	if capmode then 
		equip(sets.special.capacitypoints)
	end
end

function equip_tp_set()
	local tpmode = tpmodes[tpmodeindex]
	equip(sets.engaged[tpmode])
end

function equip_idle_set()
	equip(sets.idle.normal)
end

function self_command(command)
	local commandArgs = command
	if #commandArgs:split(' ') >= 2 then
		commandArgs = T(commandArgs:split(' '))
		parse_command(commandArgs[1], commandArgs[2], commandArgs[3])
	else
		windower.add_to_chat(123, "Error: Commands require at least 2 parameters.")
	end
end

function parse_command(param1, param2, param3)
	if param1 == "toggle" then
		handle_toggle_command(param2)
	elseif param1 == "set" then
		handle_set_command(param2, param3)
	elseif param1 == "queue" then
		handle_queue_command(param2)
	else
		windower.add_to_chat(123, "Error: First parameter not recognized.")
	end
end

function handle_toggle_command(param2)
	if param2 == "capmode" then
		capmode = not capmode
		windower.add_to_chat(123, "Capacity Point Mode set to " .. tostring(capmode) .. ".")
	elseif param2 == "placeholdermode" then
		handle_placeholder_toggle()
	else
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end	

function handle_placeholder_toggle()
	placeholdermode = not placeholdermode
	if placeholdermode then
		equip(sets.weapons.terpander)
	else
		equip(sets.weapons.gjallarhorn)
	end
	windower.add_to_chat(123, "Placeholder Mode set to " .. tostring(placeholdermode) .. ".")
end

function handle_set_command(param2, param3)
	if param2 == "tpmode" then
		if param3 == "normal" then
			tpmodeindex = 1
			windower.add_to_chat(123, "TP Mode set to NORMAL")
		elseif param3 == "dt" then
			tpmodeindex = 2
			windower.add_to_chat(123, "TP Mode set to DT")
		elseif param3 == "treasure" then
			tpmodeindex = 3
			windower.add_to_chat(123, "TP Mode set to TREASURE")
		else 
			windower.add_to_chat(123, "Error: Third parameter not recognized.")
		end
	else 
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end

function handle_queue_command(param2)
	queuedset = sets.weapons[param2]
	isqueued = true
	windower.add_to_chat(123, "Weapon in queue: " .. queuedset.name)
end