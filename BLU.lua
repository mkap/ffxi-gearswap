-- global variables
capmode = false
mbmode = true
tpmodes = {'normal', 'dt', 'treasure'}
tpmodeindex = 1;
queuedset = {}
isqueued = false

function get_sets()
	-- define empty sets
    sets.idle = {}
    sets.precast = {}
    sets.midcast = {}
    sets.aftercast = {}
    sets.engaged = {}
    sets.ws = {}
    sets.ja = {}
    sets.special = {}
    sets.weapons = {}

    -- JSE capes
    rosmerta = {}

    -- weapon sets
    sets.weapons.naeglingternion = {
    	name="Naegling / Ternion Dagger +1",
	    main="Naegling",
    	sub={ name="Ternion Dagger +1", augments={'Path: A',}},
    }

    -- idle sets
    sets.idle.normal = {

    }

    -- precast sets
    sets.precast.fastcast = {

    }

    -- midcast sets
    sets.midcast.physical = {

    }

    sets.midcast.magical = {

    }

	sets.midcast.debuff = {

	}

	sets.midcast.healing = {

	}

	-- engaged sets
    sets.engaged.normal = {

    }

	sets.engaged.dt = set_combine(sets.engaged.normal, {
		right_ring="Defending Ring",
	})

	sets.engaged.treasure = set_combine(sets.engaged.normal, {
		waist="Chaac Belt",
	})

	-- job ability sets
	sets.ja['Efflux'] = {

	}

	-- weapon skill sets
    sets.ws['Savage Blade'] = {
   
	}

	sets.ws['Chant du Cygne'] = {

	}

    sets.special.capacitypoints = {
    	back="Aptitude Mantle +1"
    }

    map_magic()
end

function map_magic()
	-- define empty sets
	magic = {}
	magic.blue = {}

	magic.blue.physical = S {
        'Asuran Claws', 'Goblin Rush'
	}

	magic.blue.magical = S {
		'Tem. Upheaval', 'Evryone. Grudge'
	}

	magic.blue.debuff = S {
		'Geist Wall', 'Blitzstrahl'
	}

	magic.blue.healing = S {
		'Healing Breeze', 'Magic Fruit', 'Plenilune Embrace'
	}
end

 
function precast(spell)
	if spell.type == "BlueMagic" or spell.type == "WhiteMagic" or spell.type == "BlackMagic" or spell.type == "Trust" then
		equip(sets.precast.fastcast)
	end
end
 
function midcast(spell)
	if spell.type == "BlueMagic" then
		midcast_bluemagic(spell)
	elseif spell.type == "JobAbility" then
		if sets.ja[spell.english] then
			equip(sets.ja[spell.english])
		end
	elseif spell.type == "WeaponSkill" then
		if sets.ws[spell.english] then
			equip(sets.ws[spell.english])
		end
	end
end

function midcast_bluemagic(spell)
	if magic.blue.physical:contains(spell.english) then
		equip(sets.midcast.physical)
	elseif magic.blue.magical:contains(spell.english) then
		equip(sets.midcast.magical)
	elseif magic.blue.debuff:contains(spell.english) then
		equip(sets.midcast.debuff)
	elseif magic.blue.healing:contains(spell.english) then
		equip(sets.midcast.healing)
	end
end

function aftercast(spell)
	handle_status_change()
	if spell.type == "WeaponSkill" and isqueued then
		equip(queuedset)
		isqueued = false
	end
end
 
function status_change(new,old)
	handle_status_change()
end

function handle_status_change()
	if player.status == "Engaged" then
		equip_tp_set()
	else
		equip_idle_set()
	end
	if capmode then 
		equip(sets.special.capacitypoints)
	end
end

function equip_tp_set()
	local tpmode = tpmodes[tpmodeindex]
	equip(sets.engaged[tpmode])
end

function equip_idle_set()
	equip(sets.idle.normal)
end

function self_command(command)
	local commandArgs = command
	if #commandArgs:split(' ') >= 2 then
		commandArgs = T(commandArgs:split(' '))
		parse_command(commandArgs[1], commandArgs[2], commandArgs[3])
	else
		windower.add_to_chat(123, "Error: Commands require at least 2 parameters.")
	end
end

function parse_command(param1, param2, param3)
	if param1 == "toggle" then
		handle_toggle_command(param2)
	elseif param1 == "set" then
		handle_set_command(param2, param3)
	elseif param1 == "queue" then
		handle_queue_command(param2)
	else
		windower.add_to_chat(123, "Error: First parameter not recognized.")
	end
end

function handle_toggle_command(param2)
	if param2 == "capmode" then
		capmode = not capmode
		windower.add_to_chat(123, "Capacity Point Mode set to " .. tostring(capmode) .. ".")
	elseif param2 == "mbmode" then
		mbmode = not mbmode
		windower.add_to_chat(123, "Magic Burst Mode set to " .. tostring(mbmode) .. ".")
	else
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end	

function handle_set_command(param2, param3)
	if param2 == "tpmode" then
		if param3 == "normal" then
			tpmodeindex = 1
			windower.add_to_chat(123, "TP Mode set to NORMAL")
		elseif param3 == "dt" then
			tpmodeindex = 2
			windower.add_to_chat(123, "TP Mode set to DT")
		elseif param3 == "treasure" then
			tpmodeindex = 3
			windower.add_to_chat(123, "TP Mode set to TREASURE")
		else 
			windower.add_to_chat(123, "Error: Third parameter not recognized.")
		end
	else 
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end

function handle_queue_command(param2)
	queuedset = sets.weapons[param2]
	isqueued = true
	windower.add_to_chat(123, "Weapon in queue: " .. queuedset.name)
end