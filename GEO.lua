-- global variables
capmode = false
tpmodes = {'normal', 'dt', 'treasure'}
idlemodes = {'normal', 'dt'}
idlemodeindex = 1
tpmodeindex = 1;

function get_sets()
    sets.idle = {}                  -- Leave this empty
    sets.precast = {}               -- Leave this empty    
    sets.midcast = {}               -- Leave this empty    
    sets.aftercast = {}             -- Leave this empty
    sets.engaged = {}				-- Leave this empty
    sets.ws = {}					-- Leave this empty
    sets.ja = {}					-- Leave this empty
    sets.special = {}				-- Leave this empty
    sets.weapons = {}				-- Leave this empty

    -- JSE capes
    nantosueltas={}
	nantosueltas.pet={}
	nantosueltas.tp={}
	nantosueltas.ws={}
	nantosueltas.magic={}
   
    -- weapon sets
    sets.weapons.gokotaitauret = {
    }

    -- idle sets
    sets.idle.normal = {
    }

    sets.idle.dt = set_combine(sets.idle.normal, {
	    right_ring="Defending Ring",
    })

    -- precast sets
    sets.precast.fastcast = {
    }

    -- midcast sets
    sets.midcast.cure = {

    }

	sets.midcast.enhancing = {
		waist="Embla Sash",
	}

    sets.midcast.elemental = {

    }

	sets.midcast.enfeebling = {

	}

	sets.midcast.geomancy = {

	}

	-- engaged sets
    sets.engaged.normal = {

    }

	sets.engaged.dt = set_combine(sets.engaged.normal, {
		right_ring="Defending Ring",
		neck="Elite royal collar",
	})

	sets.engaged.treasure = set_combine(sets.engaged.normal, {
		ammo="Per. Lucky Egg",
		waist="Chaac Belt",
	})

	-- job ability sets
	sets.ja['Full Circle'] = {

	}
	
	-- weapon skill sets
	sets.ws.debuff = {

	}

	sets.ws.magic = {

	}

	sets.ws['Hexa Strike'] = {

	}


    sets.special.capacitypoints = {
    	back="Aptitude Mantle +1"
    }
 
end
 
function precast(spell)
	if spell.type == "Ninjutsu" or spell.type == "WhiteMagic" or spell.type == "BlackMagic" or spell.type == "Trust" or spell.type == "Geomancy" then
		equip (sets.precast.fastcast)
	end
end
 
function midcast(spell)
	if spell.type == "Geomancy" then
		midcast_geomancy(spell)
	elseif spell.type == "WhiteMagic" or spell.type == "BlackMagic" then
		midcast_magic(spell)
	elseif spell.type == "JobAbility" then
		if sets.ja[spell.english] then
			equip(sets.ja[spell.english])
		end
	elseif spell.type == "WeaponSkill" then
		if sets.ws[spell.english] then
			equip(sets.ws[spell.english])
		end
	end
end

function midcast_geomancy(spell)
	equip(sets.midcast.geomancy)
end

function midcast_magic(spell)
	if spell.skill == "Enhancing Magic" then
		equip(sets.midcast.enhancing)
	elseif spell.skill == "Enfeebling Magic" then
		equip(sets.midcast.enfeebling)
	elseif spell.skill == "Elemental Magic" then
		equip(sets.midcast.elemental)
	elseif spell.skill == "Healing Magic" then
		equip(sets.midcast.cure)
	end
end

function aftercast(spell)
	handle_status_change()
end
 
function status_change(new,old)
	handle_status_change()
end

function handle_status_change()
	if player.status == "Engaged" then
		equip_tp_set()
	else
		equip_idle_set()
	end
	if capmode then 
		equip(sets.special.capacitypoints)
	end
end

function equip_tp_set()
	local tpmode = tpmodes[tpmodeindex]
	equip(sets.engaged[tpmode])
end

function equip_idle_set()
	local idlemode = idlemodes[idlemodeindex]
	equip(sets.idle[idlemode])
end

function self_command(command)
	local commandArgs = command
	if #commandArgs:split(' ') >= 2 then
		commandArgs = T(commandArgs:split(' '))
		parse_command(commandArgs[1], commandArgs[2], commandArgs[3])
	else
		windower.add_to_chat(123, "Error: Commands require at least 2 parameters.")
	end
end

function parse_command(param1, param2, param3)
	if param1 == "toggle" then
		handle_toggle_command(param2)
	elseif param1 == "set" then
		handle_set_command(param2, param3)
	elseif param1 == "queue" then
		handle_queue_command(param2)
	else
		windower.add_to_chat(123, "Error: First parameter not recognized.")
	end
end

function handle_toggle_command(param2)
	if param2 == "capmode" then
		capmode = not capmode
		windower.add_to_chat(123, "Capacity Point Mode set to " .. tostring(capmode) .. ".")
	elseif param2 == "placeholdermode" then
		handle_placeholder_toggle()
	else
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end	

function handle_placeholder_toggle()
	placeholdermode = not placeholdermode
	if placeholdermode then
		equip(sets.weapons.terpander)
	else
		equip(sets.weapons.eminentflute)
	end
	windower.add_to_chat(123, "Placeholder Mode set to " .. tostring(placeholdermode) .. ".")
end

function handle_set_command(param2, param3)
	if param2 == "tpmode" then
		if param3 == "normal" then
			tpmodeindex = 1
			windower.add_to_chat(123, "TP Mode set to NORMAL")
		elseif param3 == "dt" then
			tpmodeindex = 2
			windower.add_to_chat(123, "TP Mode set to DT")
		elseif param3 == "treasure" then
			tpmodeindex = 3
			windower.add_to_chat(123, "TP Mode set to TREASURE")
		else 
			windower.add_to_chat(123, "Error: Third parameter not recognized.")
		end
	else 
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end