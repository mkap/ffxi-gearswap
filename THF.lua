capmode = false
tpmodes = {'normal', 'dt', 'treasure'}
tpmodeindex = 1;
queuedset = {}
isqueued = false

function get_sets()
    sets.idle = {}                  -- Leave this empty
    sets.precast = {}               -- leave this empty    
    sets.midcast = {}               -- leave this empty    
    sets.aftercast = {}             -- leave this empty
    sets.engaged = {}				-- Leave this empty
    sets.ws = {}					-- Leave this empty
    sets.ja = {}					-- Leave this empty
    sets.special = {}				-- Leave this empty
    sets.weapons = {}				-- Leave this empty

    toutatis = {}
    toutatis.dexstp = { name="Toutatis's Cape", augments={'DEX+20','Accuracy+20 Attack+20','"Store TP"+10',}}
    toutatis.dexcrit = { name="Toutatis's Cape", augments={'DEX+20','Accuracy+20 Attack+20','Crit.hit rate+10',}}
    toutatis.dexwsd = { name="Toutatis's Cape", augments={'DEX+20','Accuracy+20 Attack+20','Weapon skill damage +10%',}}

    sets.weapons.sandungternion = {
	    main={ name="Sandung", augments={'Accuracy+50','Crit. hit rate+5%','"Triple Atk."+3',}},
	    sub={ name="Ternion Dagger +1", augments={'Path: A',}},
    }

    sets.idle.normal = {
	    ammo="Ginsen",
    	head="Malignance Chapeau",
    	body="Malignance Tabard",
	    hands="Malignance Gloves",
	    legs="Meg. Chausses +2",
    	feet="Jute Boots +1",
	    neck="Asperity Necklace",
	    waist="Patentia Sash",
	    left_ear="Sherida Earring",
	    right_ear="Suppanomimi",
	    left_ring="Sheltered Ring",
	    right_ring="Defending Ring",
	    back=toutatis.dexstp,
    }

    sets.precast.fastcast = {
    	ammo="White Tathlum",
	    head={ name="Herculean Helm", augments={'"Fast Cast"+6','STR+10','Mag. Acc.+4','"Mag.Atk.Bns."+2',}},
	    body={ name="Taeon Tabard", augments={'Rng.Acc.+18 Rng.Atk.+18','"Fast Cast"+4','AGI+8',}},
	    hands={ name="Herculean Gloves", augments={'"Mag.Atk.Bns."+21','"Fast Cast"+4',}},
	    legs={ name="Herculean Trousers", augments={'"Mag.Atk.Bns."+24','"Fast Cast"+6','MND+9','Mag. Acc.+7',}},
	    feet={ name="Herculean Boots", augments={'"Fast Cast"+6','"Mag.Atk.Bns."+15',}},
    	neck="Orunmila's Torque",
	    waist="Sanctuary Obi",
	    left_ear="Loquac. Earring",
	    right_ear="Eabani Earring",
	    left_ring="Weather. Ring",
	    right_ring="Defending Ring",
	    back=toutatis.dexstp,
    }

    sets.engaged.normal = {
	    ammo="Ginsen",
	    head="Malignance Chapeau",
	    body="Malignance Tabard",
	    hands="Malignance Gloves",
	    legs="Malignance Tights",
	    feet="Malignance Boots",
	    neck="Lissome Necklace",
	    waist="Patentia Sash",
	    left_ear="Eabani Earring",
	    right_ear="Suppanomimi",
	    left_ring="Epona's Ring",
	    right_ring="Chirich Ring",
	    back=toutatis.dexstp,
    }

    sets.engaged.dt = set_combine(sets.engaged.normal, {
    	neck="Elite royal collar",
		right_ring="Defending Ring",
	})

    sets.engaged.treasure = {
	    ammo="Per. Lucky Egg",
    	head="Malignance Chapeau",
    	body="Malignance Tabard",
    	hands={ name="Plun. Armlets +1", augments={'Enhances "Perfect Dodge" effect',}},
	    legs="Malignance Tights",
	    feet="Skulk. Poulaines +1",
	    neck="Asperity Necklace",
	    waist="Chaac Belt",
	    left_ear="Eabani Earring",
	    right_ear="Suppanomimi",
	    left_ring="Chirich Ring",
	    right_ring="Epona's Ring",
	    back=toutatis.dexstp,
    }

    sets.midcast.poisonga = {
	    ammo="Per. Lucky Egg",
    	head="Malignance Chapeau",
    	body="Malignance Tabard",
    	hands={ name="Plun. Armlets +1", augments={'Enhances "Perfect Dodge" effect',}},
	    legs="Meg. Chausses +2",
	    feet="Skulk. Poulaines +1",
	    neck="Asperity Necklace",
	    waist="Chaac Belt",
	    left_ear="Eabani Earring",
	    right_ear="Suppanomimi",
	    left_ring="Chirich Ring",
	    right_ring="Epona's Ring",
	    back=toutatis.dexstp,
    }

    sets.ws['Evisceration'] = {
	    ammo="Yetshila",
	    head="Mummu Bonnet +2",
	    body="Meg. Cuirie +1",
	    hands="Meg. Gloves +2",
	    legs="Mummu Kecks +2",
	    feet="Mummu Gamash. +2",
	    neck="Fotia Gorget",
	    waist="Fotia Belt",
	    left_ear="Brutal Earring",
	    right_ear="Odr Earring",
	    left_ring="Epona's Ring",
	    right_ring="Begrudging Ring",
	    back=toutatis.dexcrit,
	}

    sets.ws['Mandalic Stab'] = {
	    ammo="Voluspa Tathlum",
	    head="Meghanada Visor +1",
	    body="Meg. Cuirie +1",
	    hands="Meg. Gloves +2",
	    legs="Meg. Chausses +2",
	    feet="Meg. Jam. +1",
	    neck="Fotia Gorget",
	    waist="Fotia Belt",
	    left_ear="Mache Earring +1",
	    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    left_ring="Apate Ring",
	    right_ring="Epona's Ring",
	    back=toutatis.dexwsd,
	}

    sets.ws['Rudra\'s Storm'] = {
	    ammo="Voluspa Tathlum",
	    head="Meghanada Visor +1",
	    body="Meg. Cuirie +1",
	    hands="Meg. Gloves +2",
	    legs="Meg. Chausses +2",
	    feet="Meg. Jam. +1",
	    neck="Fotia Gorget",
	    waist="Fotia Belt",
	    left_ear="Mache Earring +1",
	    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    left_ring="Apate Ring",
	    right_ring="Epona's Ring",
	    back=toutatis.dexwsd,
	}

    sets.ws['Shark Bite'] = {
	    ammo="Voluspa Tathlum",
	    head="Meghanada Visor +1",
	    body="Meg. Cuirie +1",
	    hands="Meg. Gloves +2",
	    legs="Meg. Chausses +2",
	    feet="Meg. Jam. +1",
	    neck="Fotia Gorget",
	    waist="Fotia Belt",
	    left_ear="Mache Earring +1",
	    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    left_ring="Apate Ring",
	    right_ring="Epona's Ring",
	    back=toutatis.dexwsd,
	}

    sets.ws['Aeolian Edge'] = {
	    ammo="Ghastly Tathlum +1",
    	head={ name="Herculean Helm", augments={'"Mag.Atk.Bns."+22','Enmity-2','MND+8',}},
    	body={ name="Herculean Vest", augments={'"Mag.Atk.Bns."+24','Crit. hit damage +2%','INT+2','Mag. Acc.+5',}},
    	hands={ name="Herculean Gloves", augments={'"Mag.Atk.Bns."+25','Weapon skill damage +4%','STR+5',}},	    
    	legs={ name="Herculean Trousers", augments={'"Mag.Atk.Bns."+24','"Fast Cast"+6','MND+9','Mag. Acc.+7',}},
	    feet={ name="Herculean Boots", augments={'Mag. Acc.+11 "Mag.Atk.Bns."+11','Magic burst dmg.+5%','STR+5','"Mag.Atk.Bns."+13',}},
	    neck="Sanctity Necklace",
	    waist="Eschan Stone",
	    left_ear="Friomisi Earring",
	    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    left_ring="Shiva Ring +1",
	    right_ring="Shiva Ring +1",
	    back=toutatis.dexwsd,
	}

	sets.ja['Perfect Dodge'] = {
	    hands="Plun. Armlets +1",
	}

    sets.special.capacitypoints = {
    	back="Aptitude Mantle +1"
    }
 
end
 
function precast(spell)
	if spell.type == "WhiteMagic" or spell.type == "BlackMagic" or spell.type == "Trust" then
		equip (sets.precast.fastcast)
	end
end
 
function midcast(spell)
	if spell.type == "JobAbility" then
		if sets.ja[spell.english] then
			equip(sets.ja[spell.english])
		end
	elseif spell.type == "WeaponSkill" then
		if sets.ws[spell.english] then
			equip(sets.ws[spell.english])
		end
	elseif spell.english == "Poisonga" then
		equip(sets.midcast.poisonga)
	end
end

function aftercast(spell)
	handle_status_change()
end
 
function status_change(new,old)
	handle_status_change()
end

function handle_status_change()
	if player.status == "Engaged" then
		equip_tp_set()
	else
		equip_idle_set()
	end
	if capmode then 
		equip(sets.special.capacitypoints)
	end
end

function equip_tp_set()
	local tpmode = tpmodes[tpmodeindex]
	equip(sets.engaged[tpmode])
end

function equip_idle_set()
	equip(sets.idle.normal)
end

function self_command(command)
	local commandArgs = command
	if #commandArgs:split(' ') >= 2 then
		commandArgs = T(commandArgs:split(' '))
		parse_command(commandArgs[1], commandArgs[2], commandArgs[3])
	else
		windower.add_to_chat(123, "Error: Commands require at least 2 parameters.")
	end
end

function parse_command(param1, param2, param3)
	if param1 == "toggle" then
		handle_toggle_command(param2)
	elseif param1 == "set" then
		handle_set_command(param2, param3)
	elseif param1 == "queue" then
		handle_queue_command(param2)
	else
		windower.add_to_chat(123, "Error: First parameter not recognized.")
	end
end

function handle_toggle_command(param2)
	if param2 == "capmode" then
		capmode = not capmode
		windower.add_to_chat(123, "Capacity Point Mode set to " .. tostring(capmode) .. ".")
	else
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end	

function handle_set_command(param2, param3)
	if param2 == "tpmode" then
		if param3 == "normal" then
			tpmodeindex = 1
			windower.add_to_chat(123, "TP Mode set to NORMAL")
		elseif param3 == "dt" then
			tpmodeindex = 2
			windower.add_to_chat(123, "TP Mode set to DT")
		elseif param3 == "treasure" then
			tpmodeindex = 3
			windower.add_to_chat(123, "TP Mode set to TREASURE")
		else 
			windower.add_to_chat(123, "Error: Third parameter not recognized.")
		end
	else 
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end

function handle_queue_command(param2)
	queuedset = sets.weapons[param2]
	isqueued = true
	windower.add_to_chat(123, "Weapon in queue: " .. queuedset.main)
end