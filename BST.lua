capmode = false
mbmode = true
queuedset = {}
isqueued = false
tpmodes = {'normal', 'dt', 'treasure'}
tpmodeindex = 1;
thtag = false

function get_sets()
 
    sets.idle = {}                  -- Leave this empty
    sets.precast = {}               -- leave this empty    
    sets.midcast = {}               -- leave this empty    
    sets.aftercast = {}             -- leave this empty
    sets.engaged = {}				-- Leave this empty
    sets.ws = {}					-- Leave this empty
    sets.ja = {}					-- Leave this empty
    sets.special = {}				-- Leave this empty
    sets.weapons = {}				-- Leave this empty

    artios = {}
    artios.dexstp = { name="Artio's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','"Store TP"+10',}}
    artios.dastr = { name="Artio's Mantle", augments={'STR+20','Accuracy+20 Attack+20','"Dbl.Atk."+10',}}


    sets.weapons.apocalypse = {
	    main="Apocalypse",
    }

    sets.idle.normal = {
	    ammo="White Tathlum",
	    head="Malignance Chapeau",
	    body="Malignance Tabard",
	    hands="Malignance Gloves",
	    legs="Meg. Chausses +2",
	    feet="Malignance Boots",
	    neck="Bathy Choker +1",
	    waist="Asklepian Belt",
	    left_ear="Infused Earring",
	    right_ear="Eabani Earring",
	    left_ring="Shneddick Ring",
	    right_ring="Defending Ring",
	    back="Shadow Mantle",
    }

    sets.precast.fastcast = {

    }

    sets.ja['Ready'] = set_combine(sets.engaged.dt, {
	    legs={ name="Desultor Tassets", augments={'"Sic" and "Ready" ability delay -5','Pet: Mag. Acc.+7',}},
    })

    sets.ja['Killer Instinct'] = set_combine(sets.engaged.dt, {
    	head={ name="Ankusa Helm +1", augments={'Enhances "Killer Instinct" effect',}},
    })

    sets.ja['Reward'] = {
	    ammo="Pet Food Theta",
	    head="Skormoth Mask",
	    body="Tot. Jackcoat +1",
	    hands="Malignance Gloves",
	    legs="Tot. Trousers +1",
	    feet="Meg. Jam. +1",
	    neck="Aife's Medal",
	    waist="Salire Belt",
	    left_ear="Lifestorm Earring",
	    right_ear="Aqua Pearl",
	    left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    right_ring="Rufescent Ring",
	    back=artios.dastr,
    }

    sets.ja['Call Beast'] = set_combine(sets.engaged.dt, {
    	hands={ name="Ankusa Gloves +1", augments={'Enhances "Beast Affinity" effect',}},
    })

    sets.ja['Bestial Loyalty'] = sets.ja['Call Beast']

    sets.midcast.ready_physical = {
	    ammo="Voluspa Tathlum",
	    head="Tali'ah Turban +1",
	    body="Tali'ah Manteel +2",
	    hands="Tali'ah Gages +1",
	    legs="Tot. Trousers +1",
	    feet="Tali'ah Crackows +2",
    	neck="Empath Necklace",
	    waist="Asklepian Belt",
	    left_ear="Infused Earring",
	    right_ear="Eabani Earring",
	    left_ring="Tali'ah Ring",
	    right_ring="Defending Ring",
	    back="Shadow Mantle",
    }

    sets.midcast.ready_magicattack = {

    }

    sets.midcast.ready_magicaccuracy = {

    }

    sets.midcast.ready_multihit = {

    }

    sets.engaged.normal = {
	    ammo="Coiste Bodhar",
	    head="Malignance Chapeau",
	    body="Tali'ah Manteel +2",
	    hands="Malignance Gloves",
	    legs="Meg. Chausses +2",
	    feet="Malignance Boots",
	    neck="Anu Torque",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Eabani Earring",
	    right_ear="Suppanomimi",
	    left_ring="Epona's Ring",
	    right_ring="Rajas Ring",
	    back=artios.dexstp,
    }

	sets.engaged.dt = set_combine(sets.engaged.normal, {
		neck="Elite Royal Collar",
		right_ring="Defending Ring",
		})

    sets.engaged.treasure = {
	    ammo="Per. Lucky Egg",
	    waist="Chaac Belt",
    }

    sets.ws['Decimation'] = {
	    ammo="Coiste Bodhar",
	    head="Tali'ah Turban +1",
    	body="Nukumi Gausape +1",
	    hands="Meg. Gloves +2",
	    legs="Meg. Chausses +2",
	    feet="Tali'ah Crackows +2",
	    neck="Fotia Gorget",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Thrud Earring",
	    right_ear="Sherida Earring",
	    left_ring="Epona's Ring",
	    right_ring="Petrov Ring",
	    back=artios.dastr,
	}

    sets.ws['Mistral Axe'] = {
	    ammo="Ginsen",
	    head="Meghanada Visor +1",
    	body="Nukumi Gausape +1",
	    hands="Meg. Gloves +2",
	    legs="Meg. Chausses +2",
	    feet="Meg. Jam. +1",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Thrud Earring",
	    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    left_ring="Apate Ring",
	    right_ring="Petrov Ring",
	    back=artios.dastr,
	}

    sets.ws['Smash Axe'] = {
	    ammo="Voluspa Tathlum",
	    head="Meghanada Visor +1",
	    body="Nukumi Gausape +1",
	    hands="Meg. Gloves +2",
	    legs="Meg. Chausses +2",
	    feet="Meg. Jam. +1",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Thrud Earring",
	    right_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    left_ring="Apate Ring",
	    right_ring="Petrov Ring",
	    back=artios.dastr,
	}

    sets.ws['Calamity'] = {
	    ammo="Ginsen",
	    head="Meghanada Visor +1",
    	body="Nukumi Gausape +1",
	    hands="Meg. Gloves +2",
	    legs="Meg. Chausses +2",
	    feet="Meg. Jam. +1",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Thrud Earring",
	    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    left_ring="Apate Ring",
	    right_ring="Petrov Ring",
	    back=artios.dastr,
	}

    sets.ws['Cloudsplitter'] = {
	    ammo={ name="Ghastly Tathlum +1", augments={'Path: A',}},
	    head="Meghanada Visor +1",
	    body="Meg. Cuirie +1",
	    hands="Malignance Gloves",
	    legs="Meg. Chausses +2",
	    feet="Tali'ah Crackows +2",
	    neck="Sibyl Scarf",
	    waist="Eschan Stone",
	    left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    right_ear="Friomisi Earring",
	    left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    right_ring="Rufescent Ring",
	    back=artios.dastr,
	}

    sets.ws['Primal Rend'] = {
	    ammo={ name="Ghastly Tathlum +1", augments={'Path: A',}},
	    head="Meghanada Visor +1",
	    body="Meg. Cuirie +1",
	    hands="Malignance Gloves",
	    legs="Meg. Chausses +2",
	    feet="Tali'ah Crackows +2",
	    neck="Sibyl Scarf",
	    waist="Eschan Stone",
	    left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    right_ear="Friomisi Earring",
	    left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    right_ring="Weather. Ring",
	    back=artios.dastr,
	}

    sets.ws['Aeolian Edge'] = {
	    ammo={ name="Ghastly Tathlum +1", augments={'Path: A',}},
	    head="Meghanada Visor +1",
	    body="Meg. Cuirie +1",
	    hands="Malignance Gloves",
	    legs="Meg. Chausses +2",
	    feet="Tali'ah Crackows +2",
	    neck="Sibyl Scarf",
	    waist="Eschan Stone",
	    left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    right_ear="Friomisi Earring",
	    left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    right_ring="Rufescent Ring",
	    back=artios.dastr,
	}

	sets.special.thtag = {
	    ammo="Per. Lucky Egg",
	    waist="Chaac Belt",
	}

    sets.special.capacitypoints = {
    	back="Aptitude Mantle +1"
    }
end
 
function precast(spell)
	if spell.type == "JobAbility" then
		if sets.ja[spell.english] then
			equip(sets.ja[spell.english])
		end
	elseif spell.type == "Monster" and not spell.interrupted then
        precast_ready(spell)
	elseif spell.type == "Ninjutsu" or 
		spell.type == "WhiteMagic" or
		spell.type == "BlackMagic" or
		spell.type == "Trust" then
		equip (sets.precast.fastcast)
	end
end

function precast_ready(spell)
	equip(sets.ja['Ready'])
end
 
function midcast(spell)
	if spell.type == "Monster" and not spell.interrupted then
		midcast_monster(spell)
	elseif spell.type == "WeaponSkill" then
		if sets.ws[spell.english] then
			equip(sets.ws[spell.english])
		end
		if thtag then
			equip(sets.special.thtag)
			thtag = false
		end
	end
end

function midcast_monster(spell)
	if physical_ready_moves:contains(spell.english) then
		equip(sets.midcast.ready_physical)
	elseif magic_atk_ready_moves:contains(spell.english) then
		equip(sets.midcast.ready_magicattack) 
	elseif magic_acc_ready_moves:contains(spell.english) then
		equip(sets.midcast.ready_magicaccuracy) 
	elseif multi_hit_ready_moves:contains(spell.english) then
		equip(sets.midcast.ready_multihit) 
	end
end
 
function aftercast(spell)
	handle_status_change()
	if spell.type == "WeaponSkill" and isqueued then
		equip(queuedset)
		isqueued = false
	end
end
 
function status_change(new,old)
	handle_status_change()
end

function handle_status_change()
	if player.status == "Engaged" then
		equip_tp_set()
	else
		equip_idle_set()
	end
	if capmode then 
		equip(sets.special.capacitypoints)
	end
end

function equip_tp_set()
	local tpmode = tpmodes[tpmodeindex]
	equip(sets.engaged[tpmode])
end

function equip_idle_set()
	equip(sets.idle.normal)
end

function self_command(command)
	local commandArgs = command
	commandArgs = T(commandArgs:split(' '))
	parse_command(commandArgs[1], commandArgs[2], commandArgs[3])
end

function parse_command(param1, param2, param3)
	if param1 == "toggle" then
		handle_toggle_command(param2)
	elseif param1 == "set" then
		handle_set_command(param2, param3)
	elseif param1 == "queue" then
		handle_queue_command(param2)
	elseif param1 == "thtag" then
		thtag = true
		windower.add_to_chat(123, "Treasure Hunter Tag enabled.")
	else
		windower.add_to_chat(123, "Error: First parameter not recognized.")
	end
end

function handle_toggle_command(param2)
	if param2 == "capmode" then
		capmode = not capmode
		windower.add_to_chat(123, "Capacity Point Mode set to " .. tostring(capmode) .. ".")
	elseif param2 == "mbmode" then
		mbmode = not mbmode
		windower.add_to_chat(123, "Magic Burst Mode set to " .. tostring(mbmode) .. ".")
	else
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end	

function handle_set_command(param2, param3)
	if param2 == "tpmode" then
		if param3 == "normal" then
			tpmodeindex = 1
			windower.add_to_chat(123, "TP Mode set to NORMAL")
		elseif param3 == "dt" then
			tpmodeindex = 2
			windower.add_to_chat(123, "TP Mode set to DT")
		elseif param3 == "treasure" then
			tpmodeindex = 3
			windower.add_to_chat(123, "TP Mode set to TREASURE")
		else 
			windower.add_to_chat(123, "Error: Third parameter not recognized.")
		end
	else 
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end

function handle_queue_command(param2)
	queuedset = sets.weapons[param2]
	isqueued = true
	windower.add_to_chat(123, "Weapon in queue: " .. queuedset.main)
end

physical_ready_moves = S{'Foot Kick','Whirl Claws','Sheep Charge','Lamb Chop','Head Butt','Wild Oats',
    'Leaf Dagger','Claw Cyclone','Razor Fang','Crossthrash','Nimble Snap','Cyclotail','Rhino Attack',
    'Power Attack','Mandibular Bite','Big Scissors','Grapple','Spinning Top','Double Claw','Frogkick',
    'Blockhead','Brain Crush','Tail Blow','Scythe Tail','Ripper Fang','Chomp Rush','Needleshot',
    'Recoil Dive','Sudden Lunge','Spiral Spin','Wing Slap','Beak Lunge','Suction','Back Heel',
    'Fantod','Tortoise Stomp','Sensilla Blades','Tegmina Buffet','Pentapeck','Sweeping Gouge',
    'Somersault','Tickling Tendrils','Pecking Flurry','Sickle Slash','Disembowel','Extirpating Salvo',
    'Mega Scissors','Rhinowrecker','Hoof Volley','Fluid Toss','Fluid Spread'
}

magic_atk_ready_moves = S{'Dust Cloud','Cursed Sphere','Venom','Toxic Spit','Bubble Shower','Drainkiss',
    'Silence Gas','Dark Spore','Fireball','Plague Breath','Snow Cloud','Charged Whisker','Corrosive Ooze',
    'Aqua Breath','Stink Bomb','Nectarous Deluge','Nepenthic Plunge','Pestilent Plume','Foul Waters',
    'Acid Spray','Infected Leech','Gloom Spray','Venom Shower'
}

magic_acc_ready_moves = S{'Sheep Song','Scream','Dream Flower','Roar','Predatory Glare','Gloeosuccus',
    'Palsy Pollen','Soporific','Geist Wall','Toxic Spit','Numbing Noise','Spoil','Hi-Freq Field',
    'Sandpit','Sandblast','Venom Spray','Filamented Hold','Queasyshroom','Numbshroom','Spore','Shakeshroom',
    'Infrasonics','Chaotic Eye','Blaster','Purulent Ooze','Intimidate','Noisome Powder','Acid Mist',
    'Choke Breath','Jettatura','Nihility Song','Molting Plumage','Swooping Frenzy','Spider Web'
}

multi_hit_ready_moves = S{'Pentapeck','Tickling Tendrils','Sweeping Gouge','Chomp Rush','Wing Slap',
    'Pecking Flurry'
}	

call_beast_cancel = S{
	'Vis. Broth',
	'Ferm. Broth',
	'Bubbly Broth',
	'Windy Greens',
	'Bug-Ridden Broth',
	'Tant. Broth',
    'Glazed Broth',
	'Slimy Webbing',
	'Deepwater Broth',
	'Venomous Broth',
	'Heavenly Broth',
	'Pungent Broth',
	'Putrescent Broth',
}