capmode = false
mbmode = true
queuedset = {}
isqueued = false
tpmodes = {'normal', 'dt', 'treasure'}
tpmodeindex = 1
thtag = false
am3 = false

function get_sets()
 
    sets.idle = {}                  -- Leave this empty
    sets.precast = {}               -- leave this empty    
    sets.midcast = {}               -- leave this empty    
    sets.aftercast = {}             -- leave this empty
    sets.engaged = {}				-- Leave this empty
    sets.ws = {}					-- Leave this empty
    sets.ja = {}					-- Leave this empty
    sets.special = {}				-- Leave this empty
    sets.weapons = {}				-- Leave this empty

    ankou = {}
    ankou.dadex = { name="Ankou's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Phys. dmg. taken-10%',}}
    ankou.wsdstr = { name="Ankou's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}}
    ankou.wsdvit = { name="Ankou's Mantle", augments={'VIT+20','Accuracy+20 Attack+20','Weapon skill damage +10%',}}
    ankou.fastcast = { name="Ankou's Mantle", augments={'"Fast Cast"+10',}}
    ankou.stpdex = { name="Ankou's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Store TP"+10','Phys. dmg. taken-10%',}}

    niht = {}
    niht.drain = { name="Niht Mantle", augments={'Attack+12','Dark magic skill +10','"Drain" and "Aspir" potency +23',}}

    sets.weapons.apocalypse = {
	    main="Apocalypse",
    }

    sets.weapons.raeticalgol = {
    	main="Raetic Algol +1",
    }

    sets.weapons.lycurgos = {
    	main="Lycurgos",
    }

    sets.idle.normal = {
	    ammo="White Tathlum",
    	body="Lugra Cloak +1",
        hands="Flamma Manopolas +2",
    	legs="Carmine Cuisses +1",
	    feet="Flam. Gambieras +2",
	    neck="Bathy Choker +1",
	    waist="Eschan Stone",
	    left_ear="Infused Earring", 
	    right_ear="Brutal Earring",
	    left_ring="Sheltered Ring",
	    right_ring="Defending Ring",
	    back="Archon Cape",
    }

    sets.precast.fastcast = {
	    ammo="White Tathlum",
	    head={ name="Carmine Mask +1", augments={'Accuracy+20','Mag. Acc.+12','"Fast Cast"+4',}},
    	body={ name="Odyss. Chestplate", augments={'Mag. Acc.+15','"Fast Cast"+3','CHR+10','"Mag.Atk.Bns."+2',}},
	    hands="Sulev. Gauntlets +2",
	    legs={ name="Odyssean Cuisses", augments={'Mag. Acc.+1','"Fast Cast"+6','INT+4','"Mag.Atk.Bns."+3',}},
	    feet={ name="Odyssean Greaves", augments={'"Fast Cast"+6','AGI+4','Mag. Acc.+5','"Mag.Atk.Bns."+6',}},
	    neck="Orunmila's Torque",
	    waist="Asklepian Belt",
	    left_ear="Loquac. Earring",
	    right_ear="Malignance Earring",
	    left_ring="Weather. Ring",
	    right_ring="Kishar Ring",
    	back=ankou.fastcast,
    }

    sets.ja['Arcane Circle'] = {
    	feet="Igno. Sollerets +1",
    }

    sets.ja['Weapon Bash'] = {
    	hands="Ig. Gauntlets +2",
    }

    sets.ja['Nether Void'] = {
    	legs="Heath. Flanchard +2",
    }

    sets.midcast.elemental = {
	    ammo={ name="Seeth. Bomblet +1", augments={'Path: A',}},
	    head="Flam. Zucchetto +2",
	    body="Flamma Korazin +2",
	    hands="Flam. Manopolas +2",
	    legs="Flamma Dirs +2",
	    feet="Flam. Gambieras +2",
	    neck="Erra Pendant",
	    waist="Eschan Stone",
	    left_ear="Friomisi Earring",
	    right_ear="Hecate's Earring",
	    left_ring="Shiva Ring +1",
	    right_ring="Mujin Band",
	    back=niht.drain,
    }

    sets.midcast.dark = {
	    ammo={ name="Seeth. Bomblet +1", augments={'Path: A',}},
	    head="Flam. Zucchetto +2",
	    body="Flamma Korazin +2",
	    hands="Flam. Manopolas +2",
	    legs="Flamma Dirs +2",
	    feet="Flam. Gambieras +2",
	    neck="Erra Pendant",
	    waist="Eschan Stone",
	    left_ear="Gwati Earring",
	    right_ear="Malignance Earring",
	    left_ring="Vertigo Ring",
	    right_ring="Flamma Ring",
	    back=niht.drain,
    }

    sets.midcast.enfeebling = {
	    ammo={ name="Seeth. Bomblet +1", augments={'Path: A',}},
	    head="Flam. Zucchetto +2",
	    body="Flamma Korazin +2",
	    hands="Flam. Manopolas +2",
	    legs="Flamma Dirs +2",
	    feet="Flam. Gambieras +2",
	    neck="Erra Pendant",
	    waist="Eschan Stone",
	    left_ear="Gwati Earring",
	    right_ear="Malignance Earring",
	    left_ring="Vertigo Ring",
	    right_ring="Flamma Ring",
	    back=niht.drain,
    }

    sets.midcast.drain = {
	    ammo="Hydrocera",
    	head={ name="Fall. Burgeonet +2", augments={'Enhances "Dark Seal" effect',}},
    	body={ name="Carm. Sc. Mail +1", augments={'MP+80','INT+12','MND+12',}},
    	hands={ name="Fall. Fin. Gaunt. +2", augments={'Enhances "Diabolic Eye" effect',}},
    	legs="Heath. Flanchard +2",
	    feet="Ratri Sollerets",
	    neck="Erra Pendant",
	    waist="Eschan Stone",
	    left_ear="Hirudinea Earring",
	    right_ear="Malignance Earring",
	    left_ring="Archon Ring",
	    right_ring="Evanescence Ring",
	    back=niht.drain,
    }

    sets.midcast.absorb = {
	    ammo="Hydrocera",
	    head="Ig. Burgeonet +2",
    	body={ name="Carm. Sc. Mail +1", augments={'MP+80','INT+12','MND+12',}},
    	hands={ name="Fall. Fin. Gaunt. +2", augments={'Enhances "Diabolic Eye" effect',}},
    	legs="Heath. Flanchard +2",
	    feet="Ratri Sollerets",
	    neck="Erra Pendant",
	    waist="Casso Sash",
	    left_ear="Mani Earring",
	    right_ear="Dark Earring",
	    left_ring="Kishar Ring",
	    right_ring="Stikini Ring",
	    back="Chuparrosa Mantle",
	    back=ankou.dadex,
    }

    sets.midcast.endark = {
	    ammo="Hydrocera",
	    head="Ig. Burgeonet +2",
    	body={ name="Carm. Sc. Mail +1", augments={'MP+80','INT+12','MND+12',}},
	    hands={ name="Fall. Fin. Gaunt. +2", augments={'Enhances "Diabolic Eye" effect',}},
    	legs="Heath. Flanchard +2",
	    feet="Ratri Sollerets",
	    neck="Erra Pendant",
	    waist="Casso Sash",
	    left_ear="Mani Earring",
	    right_ear="Dark Earring",
	    left_ring="Stikini Ring",
	    right_ring="Evanescence Ring",
	    back=niht.drain,
    }

    sets.midcast.dreadspikes = {
	    ammo="White Tathlum",
	    head={ name="Fall. Burgeonet +2", augments={'Enhances "Dark Seal" effect',}},
    	body="Heath. Cuirass +2",
	    hands="Ratri Gadlings",
	    legs="Flamma Dirs +2",
	    feet="Ratri Sollerets",
	    neck="Sanctity Necklace",
	    waist="Eschan Stone",
	    left_ear="Eabani Earring",
	    right_ear="Assuage Earring",
	    left_ring="Rhodium Ring",
	    right_ring="Prouesse Ring",
	    back=niht.drain,
    }

    sets.midcast.poisonga = set_combine(sets.midcast.enfeebling, {
	    ammo="Per. Lucky Egg",
    	body={ name="Valorous Mail", augments={'Mag. Acc.+18','Accuracy+7','"Treasure Hunter"+2','Mag. Acc.+8 "Mag.Atk.Bns."+8',}},
	    waist="Chaac Belt",
	})

    sets.engaged.normal = {
	    ammo="Coiste Bodhar",
	    head="Flam. Zucchetto +2",
	    body={ name="Valorous Mail", augments={'Attack+20','"Store TP"+8',}},
	    hands="Sulev. Gauntlets +2",
    	legs="Ig. Flanchard +3",
	    feet="Flam. Gambieras +2",
	    neck="Asperity Necklace",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Cessance Earring",
	    right_ear="Brutal Earring",
	    left_ring="Chirich Ring",
	    right_ring="Rajas Ring",
	    back=ankou.dadex,
    }

    sets.engaged.normal.libam3 = {
	    ammo="Coiste Bodhar",
	    head="Flam. Zucchetto +2",
	    body={ name="Valorous Mail", augments={'Attack+20','"Store TP"+8',}},
    	hands="Flam. Manopolas +2",
	    legs={ name="Odyssean Cuisses", augments={'"Store TP"+8',}},
    	feet={ name="Valorous Greaves", augments={'"Mag.Atk.Bns."+20','"Store TP"+5','VIT+6','Accuracy+11',}},
    	neck="Lissome Necklace",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Cessance Earring",
	    right_ear="Brutal Earring",
	    left_ring="Chirich Ring",
	    right_ring="Rajas Ring",
	    back=ankou.stpdex,
    }

	sets.engaged.dt = set_combine(sets.engaged.normal, {
		head="Sulevia's Mask +2",
		neck="Elite Royal Collar",
		left_ring={ name="Gelatinous Ring +1", augments={'Path: A',}},
		right_ring="Defending Ring",
		})

	sets.engaged.dt.libam3 = set_combine(sets.engaged.normal.libam3, {
		head="Sulevia's Mask +2",
		neck="Elite Royal Collar",
		left_ring={ name="Gelatinous Ring +1", augments={'Path: A',}},
		right_ring="Defending Ring",
		})


    sets.engaged.treasure = {
	    ammo="Per. Lucky Egg",
	    head="Flam. Zucchetto +2",
    	body={ name="Valorous Mail", augments={'STR+5','Enmity-2','"Treasure Hunter"+1','Accuracy+17 Attack+17','Mag. Acc.+4 "Mag.Atk.Bns."+4',}},
    	hands="Sulev. Gauntlets +2",
	    legs="Flamma Dirs +2",
	    feet="Flam. Gambieras +2",
	    neck="Asperity Necklace",
	    waist="Chaac Belt",
	    left_ear="Mache Earring +1",
	    right_ear="Brutal Earring",
	    left_ring="Chirich Ring",
    	right_ring="Rajas Ring",
	    back=ankou.dadex,
    }

	sets.engaged.treasure.libam3 = sets.engaged.treasure

	sets.ws.debuff = {
	    ammo={ name="Seeth. Bomblet +1", augments={'Path: A',}},
	    head="Sulevia's Mask +2",
	    body="Sulevia's Plate. +2",
	    hands="Sulev. Gauntlets +2",
	    legs="Sulevi. Cuisses +2",
	    feet="Heath. Sollerets +2",
	    neck="Fotia Gorget",
	    waist="Eschan Stone",
	    left_ear="Malignance Earring",
	    right_ear="Gwati Earring",
	    left_ring="Apate Ring",
	    right_ring="Rajas Ring",
	    back=ankou.wsdstr,    
	}

    sets.ws['Cross Reaper'] = {
	    ammo="Knobkierrie",
	    head="Sulevia's Mask +2",
	    body="Ignominy Cuirass +3",
	    hands="Sulev. Gauntlets +2",
	    legs="Sulev. Cuisses +2",
	    feet="Heath. Sollerets +2",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear="Thrud Earring",
	    left_ring="Apate Ring",
	    right_ring="Rufescent Ring",
	    back=ankou.wsdstr,    
	}

    sets.ws['Catastrophe'] = {
	    ammo="Knobkierrie",
	    head={ name="Fall. Burgeonet +2", augments={'Enhances "Dark Seal" effect',}},
	    body="Ignominy Cuirass +3",
	    hands={ name="Fall. Fin. Gaunt. +2", augments={'Enhances "Diabolic Eye" effect',}},
	    legs="Ig. Flanchard +3",
	    feet="Heath. Sollerets +2",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    right_ear="Thrud Earring",
	    left_ring="Rufescent Ring",
	    right_ring="Apate Ring",
	    back=ankou.wsdstr,    
	}

    sets.ws['Quietus'] = sets.ws['Catastrophe']
    sets.ws['Guillotine'] = sets.ws['Catastrophe']

    sets.ws['Insurgency'] = {
	    ammo="Knobkierrie",
	    head={ name="Fall. Burgeonet +2", augments={'Enhances "Dark Seal" effect',}},
	    body="Ignominy Cuirass +3",
	    hands={ name="Fall. Fin. Gaunt. +2", augments={'Enhances "Diabolic Eye" effect',}},
	    legs="Ratri Cuisses",
	    feet="Heath. Sollerets +2",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    right_ear="Thrud Earring",
	    left_ring="Apate Ring",
	    right_ring="Rajas Ring",
	    back=ankou.wsdstr,    
	}

    sets.ws['Entropy'] = {
	    ammo={ name="Seeth. Bomblet +1", augments={'Path: A',}},
	    head={ name="Fall. Burgeonet +2", augments={'Enhances "Dark Seal" effect',}},
	    body="Ignominy Cuirass +3",
	    hands={ name="Fall. Fin. Gaunt. +2", augments={'Enhances "Diabolic Eye" effect',}},
	    legs="Ig. Flanchard +3",
	    feet="Heath. Sollerets +2",
	    neck="Fotia Gorget",
	    waist="Fotia Belt",
	    left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    right_ear="Thrud Earring",
	    left_ring="Shiva Ring +1",
	    right_ring="Shiva Ring +1",
	    back=ankou.wsdstr,
    }

    sets.ws['Infernal Scythe'] = {
    	ammo={ name="Seeth. Bomblet +1", augments={'Path: A',}},
	    head="Sulevia's Mask +2",
	    body="Sulevia's Plate. +2",
	    hands="Sulev. Gauntlets +2",
	    legs="Sulevi. Cuisses +2",
	    feet="Heath. Sollerets +2",
	    neck="Fotia Gorget",
	    waist="Fotia Belt",
	    left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    right_ear="Thrud Earring",
	    left_ring="Shiva Ring +1",
	    right_ring="Shiva Ring +1",
	    back=ankou.wsdstr,
    }

    sets.ws['Spinning Scythe'] = {
	    ammo="Knobkierrie",
	    head="Sulevia's Mask +2",
	    body="Ignominy Cuirass +3",
	    hands="Sulev. Gauntlets +2",
	    legs="Sulev. Cuisses +2",
	    feet="Heath. Sollerets +2",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear="Thrud Earring",
	    left_ring="Apate Ring",
	    right_ring="Rajas Ring",
	    back=ankou.wsdstr,    
	}

    sets.ws['Armor Break'] = sets.ws.debuff
    sets.ws['Shield Break'] = sets.ws.debuff
    sets.ws['Weapon Break'] = sets.ws.debuff
    sets.ws['Nightmare Scythe'] = sets.ws.debuff
    sets.ws['Infernal Scythe'] = sets.ws.debuff
    sets.ws['Shockwave'] = sets.ws.debuff

    sets.ws['Torcleaver'] = {
	    ammo="Knobkierrie",
	    head="Sulevia's Mask +2",
	    body="Ignominy Cuirass +3",
	    hands="Sulev. Gauntlets +2",
	    legs="Sulev. Cuisses +2",
	    feet="Heath. Sollerets +2",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear="Thrud Earring",
	    left_ring="Petrov Ring",
	    right_ring={ name="Gelatinous Ring +1", augments={'Path: A',}},
	    back=ankou.wsdvit,    
	}

    sets.ws['Resolution'] = {
	    ammo="Knobkierrie",
	    head="Sulevia's Mask +2",
	    body="Ignominy Cuirass +3",
	    hands="Sulev. Gauntlets +2",
	    legs="Sulev. Cuisses +2",
	    feet="Heath. Sollerets +2",
	    neck="Fotia Gorget",
	    waist="Fotia Belt",
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear="Thrud Earring",
	    left_ring="Apate Ring",
	    right_ring="Rajas Ring",
	    back=ankou.wsdstr,    
	}

    sets.ws['Steel Cyclone'] = {
	    ammo="Knobkierrie",
	    head="Sulevia's Mask +2",
	    body="Ignominy Cuirass +3",
	    hands="Sulev. Gauntlets +2",
	    legs="Sulev. Cuisses +2",
	    feet="Heath. Sollerets +2",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear="Thrud Earring",
	    left_ring="Apate Ring",
	    right_ring="Rajas Ring",
	    back=ankou.wsdstr,    
	}

    sets.ws['Upheaval'] = {
	    ammo="Knobkierrie",
	    head="Sulevia's Mask +2",
	    body="Ignominy Cuirass +3",
	    hands="Sulev. Gauntlets +2",
	    legs="Sulev. Cuisses +2",
	    feet="Heath. Sollerets +2",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear="Thrud Earring",
	    left_ring="Petrov Ring",
	    right_ring={ name="Gelatinous Ring +1", augments={'Path: A',}},
	    back=ankou.wsdvit,    
	}

    sets.ws['Fell Cleave'] = {
	    ammo="Knobkierrie",
	    head={ name="Fall. Burgeonet +2", augments={'Enhances "Dark Seal" effect',}},
	    body="Ignominy Cuirass +3",
	    hands={ name="Fall. Fin. Gaunt. +2", augments={'Enhances "Diabolic Eye" effect',}},
	    legs="Ig. Flanchard +3",
	    feet="Heath. Sollerets +2",
	    neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear="Thrud Earring",
	    left_ring="Apate Ring",
	    right_ring="Rajas Ring",
	    back=ankou.wsdstr,    
	}

	sets.special.thtag = {
	    ammo="Per. Lucky Egg",
    	body={ name="Valorous Mail", augments={'Mag. Acc.+18','Accuracy+7','"Treasure Hunter"+2','Mag. Acc.+8 "Mag.Atk.Bns."+8',}},
	    waist="Chaac Belt",
	}

    sets.special.capacitypoints = {
    	back="Aptitude Mantle +1"
    }
 
end
 
function precast(spell)
	if spell.type == "WhiteMagic" or
		spell.type == "BlackMagic" or
		spell.type == "Trust" then
		equip (sets.precast.fastcast)
	end
end
 
function midcast(spell)
	if spell.english:startswith('Drain') or spell.english:startswith('Aspir') then
		equip(sets.midcast.drain)
	elseif spell.english:startswith('Absorb') then
		equip(sets.midcast.absorb)
	elseif spell.english:startswith('Endark') then
		equip(sets.midcast.endark)
	elseif spell.english == "Dread Spikes" then
		equip(sets.midcast.dreadspikes)
	elseif spell.english == "Poisonga" then
		equip(sets.midcast.poisonga)
	elseif spell.skill == "Elemental Magic" then
		equip(sets.midcast.elemental)
	elseif spell.skill == "Enfeebling Magic" then
		equip(sets.midcast.enfeebling)
	elseif spell.type == "JobAbility" then
		if sets.ja[spell.english] then
			equip(sets.ja[spell.english])
		end
	elseif spell.type == "WeaponSkill" then
		if sets.ws[spell.english] then
			equip(sets.ws[spell.english])
		end
		if thtag then
			equip(sets.special.thtag)
			thtag = false
		end
	end
end
 
function aftercast(spell)
	handle_status_change()
	if spell.type == "WeaponSkill" and isqueued then
		equip(queuedset)
		isqueued = false
	end
end
 
function status_change(new,old)
	handle_status_change()
end

function buff_change(buff,gain)
    if player.equipment.main == "Liberator" and buff == "Aftermath: Lv.3" then
        if gain then
        	am3 = true
        else
        	am3 = false
        end
    end
end

function handle_status_change()
	if player.status == "Engaged" then
		equip_tp_set()
	else
		equip_idle_set()
	end
	if capmode then 
		equip(sets.special.capacitypoints)
	end
end

function equip_tp_set()
	local tpmode = tpmodes[tpmodeindex]
	if am3 == true then
		equip(sets.engaged[tpmode].libam3)
	else
		equip(sets.engaged[tpmode])
	end

end

function equip_idle_set()
	equip(sets.idle.normal)
end

function self_command(command)
	local commandArgs = command
	commandArgs = T(commandArgs:split(' '))
	parse_command(commandArgs[1], commandArgs[2], commandArgs[3])
end

function parse_command(param1, param2, param3)
	if param1 == "toggle" then
		handle_toggle_command(param2)
	elseif param1 == "set" then
		handle_set_command(param2, param3)
	elseif param1 == "queue" then
		handle_queue_command(param2)
	elseif param1 == "thtag" then
		thtag = true
		windower.add_to_chat(123, "Treasure Hunter Tag enabled.")
	else
		windower.add_to_chat(123, "Error: First parameter not recognized.")
	end
end

function handle_toggle_command(param2)
	if param2 == "capmode" then
		capmode = not capmode
		windower.add_to_chat(123, "Capacity Point Mode set to " .. tostring(capmode) .. ".")
	elseif param2 == "mbmode" then
		mbmode = not mbmode
		windower.add_to_chat(123, "Magic Burst Mode set to " .. tostring(mbmode) .. ".")
	else
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end	

function handle_set_command(param2, param3)
	if param2 == "tpmode" then
		if param3 == "normal" then
			tpmodeindex = 1
			windower.add_to_chat(123, "TP Mode set to NORMAL")
		elseif param3 == "dt" then
			tpmodeindex = 2
			windower.add_to_chat(123, "TP Mode set to DT")
		elseif param3 == "treasure" then
			tpmodeindex = 3
			windower.add_to_chat(123, "TP Mode set to TREASURE")
		else 
			windower.add_to_chat(123, "Error: Third parameter not recognized.")
		end
	else 
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end

function handle_queue_command(param2)
	queuedset = sets.weapons[param2]
	isqueued = true
	windower.add_to_chat(123, "Weapon in queue: " .. queuedset.main)
end